# Smart AdServer — Core SDK

The _Smart AdServer Core SDK is a dependency used by several other _Smart AdServer_ frameworks. You should never install it or use it directly.

For more information about _Smart AdServer_ products, check our website:

> https://smartadserver.com

## Requirements

* _Xcode 14.0_ or higher
* _iOS 11.0_ or higher
* _tvOS 12.0_ or higher
